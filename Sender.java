import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.Random;

public class Sender {

    private Random generator;
    private String receiverIP;
    private int receiverPort;
    private String file;
    private int mws;
    private int mss;
    private int timeout;
    private float pdrop;
    private long seed;
    private int senderPort;
    private long startTime;
    private PrintWriter writer;
    private int seqNumber;
    private int ackNumber;

    public Sender () {}

    public static void main(String[] args) {

        if (args.length != 8) {
            System.out.println("Required arguments: receiver_host_ip receiver_port file.txt MWS MSS timeout pdrop seed");
            return;
        }

        Sender sender = new Sender();

        sender.startTime = (long) System.currentTimeMillis();
        sender.receiverIP = args[0];
        sender.receiverPort = Integer.parseInt(args[1]);
        sender.file = args[2];
        sender.mws = Integer.parseInt(args[3]);
        sender.mss = Integer.parseInt(args[4]);
        sender.timeout = Integer.parseInt(args[5]);
        sender.pdrop = Float.parseFloat(args[6]);
        System.out.println(sender.pdrop);
        sender.seed = Long.parseLong(args[7]);
        sender.seqNumber = 0;
        sender.ackNumber = 0;

        try {
            sender.writer = new PrintWriter("Sender_log.txt", "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ServerSocket socket = new ServerSocket(0);
            sender.senderPort = socket.getLocalPort();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            DatagramSocket ds = new DatagramSocket();
            ds.connect(new InetSocketAddress(sender.receiverIP, sender.receiverPort));
            ds.setSoTimeout(sender.timeout);
            sender.handshake(ds);
            sender.transporter(ds);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sender.writer.close();
    }

    public void handshake (DatagramSocket ds) {
        int seqNum = Math.abs(new Random(System.currentTimeMillis()).nextInt(9999-1)+1);
        byte[] dataToSend1 = encode("S", seqNum, 0, this.senderPort, this.receiverPort, "");

        try {
            DatagramPacket toSend1 = new DatagramPacket(dataToSend1, dataToSend1.length, InetAddress.getByName(this.receiverIP), this.receiverPort);
            sender(ds, toSend1, "S", seqNum, 0, 0);
        } catch (Exception e) {
            System.out.println("error");
        }

        DatagramPacket toReceive = new DatagramPacket(new byte[50], 50);
        toReceive = receiver(ds, toReceive);
        String[] synack = {};

        try {
            toReceive = receiver(ds, toReceive);
            synack = decode(toReceive.getData());
            //writer.println("rcv " + (System.currentTimeMillis()-this.startTime) + " " + synack[0] + " " +synack[1] + " " + synack[2] + synack[3]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            byte[] dataToSend2 = encode("A", Integer.parseInt(synack[3]), Integer.parseInt(synack[1])+1, this.senderPort, this.receiverPort, "");
            DatagramPacket toSend2 = new DatagramPacket(dataToSend2, dataToSend2.length, InetAddress.getByName(this.receiverIP), this.receiverPort);
            sender(ds, toSend2, "A", Integer.parseInt(synack[3]), Integer.parseInt(synack[1])+1, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.seqNumber = Integer.parseInt(synack[3]);
        this.ackNumber = Integer.parseInt(synack[1])+1;
    }

    public void transporter(DatagramSocket ds) {
        byte[] chunk = new byte[this.mss];
        byte[] data;
        String[] received;
        InputStream is = null;
        try {
                is = new FileInputStream(this.file);
        } catch (Exception e) {}
        int seqNum = this.seqNumber;
        int ackNum = this.ackNumber;
        int read = -1;
        DatagramPacket dp1;
        DatagramPacket dp2 = new DatagramPacket(new byte[this.mss+100], this.mss+100);

        try {
            //for (int x = 0; x < this.mws/this.mss; x++) {
            while((read = is.read(chunk, 0, chunk.length)) != -1) {
                while (read < chunk.length) {
                    String end = new String("\0");
                    chunk[read] = end.getBytes()[0];
                    read++;
                }
                
                //System.out.print(new String(chunk));
                data = encode("D", seqNum, ackNum, this.senderPort, this.receiverPort, new String(chunk));
                dp1 = new DatagramPacket(data, data.length, InetAddress.getByName(this.receiverIP), this.receiverPort);
                sender(ds, dp1, "D", seqNum, ackNum, chunk.length);
                //}

                dp2 = receiver(ds, dp2);
                received = decode(dp2.getData());
                //System.out.println(Arrays.toString(decode(chunk)));
                seqNum = Integer.parseInt(received[3]);
                ackNum = Integer.parseInt(received[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.seqNumber = seqNum;
        this.ackNumber = ackNum;
        teardown(ds);
    }

    public void teardown(DatagramSocket ds) {
        byte[] dataToSend1 = encode("F", this.seqNumber, this.ackNumber, this.senderPort, this.receiverPort, "");

        try {
            DatagramPacket toSend1 = new DatagramPacket(dataToSend1, dataToSend1.length, InetAddress.getByName(this.receiverIP), this.receiverPort);
            sender(ds, toSend1, "F", this.seqNumber, this.ackNumber, 0);
        } catch (Exception e) {
            System.out.println("error");
        }

        DatagramPacket toReceive = new DatagramPacket(new byte[50], 50);
        toReceive = receiver(ds, toReceive);
        String[] fin = {};

        try {
            toReceive = receiver(ds, toReceive);
            toReceive = receiver(ds, toReceive);
            fin = decode(toReceive.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            byte[] dataToSend2 = encode("A", Integer.parseInt(fin[3]), Integer.parseInt(fin[1])+1, this.senderPort, this.receiverPort, "");
            DatagramPacket toSend2 = new DatagramPacket(dataToSend2, dataToSend2.length, InetAddress.getByName(this.receiverIP), this.receiverPort);
            sender(ds, toSend2, "A", Integer.parseInt(fin[3]), Integer.parseInt(fin[1])+1, 0);
            ds.close();
        } catch (Exception e) {}
    }

    public byte[] encode(String type, int seqNum, int ackNum, int sourcePort, int destPort, String data) {
        //System.out.println(new String(type + " " + Integer.toString(seqNum) + " " + Integer.toString(data.length()) + " " + Integer.toString(ackNum)).getBytes());
        return new String(type + " " + Integer.toString(seqNum) + " " + Integer.toString(data.length()) + " " + Integer.toString(ackNum) + " " + Integer.toString(sourcePort) + " " + Integer.toString(destPort) + " " + data).getBytes();
    }

    public String[] decode (byte[] packet) {
        return new String(packet).split(" ", 7);
    }

    public void sender (DatagramSocket s, DatagramPacket p, String type, int seqNum, int ackNum, int length) {
        try {
            if (type.equals("D")) {
                if (pld()) {
                    s.send(p);
                    this.writer.println("snd " + (System.currentTimeMillis()-this.startTime) + " " + type + " " + seqNum + " " + length + " " + ackNum);
                } else {
                    this.writer.println("drop " + (System.currentTimeMillis()-this.startTime) + " " + type + " " + seqNum + " " + length + " " + ackNum);
                    //sender(s, p, type, seqNum, ackNum, length);
                    s.send(p);
                }
            } else {
                s.send(p);
                this.writer.println("snd " + (System.currentTimeMillis()-this.startTime) + " " + type + " " + seqNum + " " + length + " " + ackNum);
            }
            return;
        } catch (Exception e) {
            //System.out.println("error sending");
            this.writer.println("drop " + (System.currentTimeMillis()-this.startTime) + " " + type + " " + seqNum + " " + length + " " + ackNum);
            sender(s, p, type, seqNum, ackNum, length);
            return;
        }
    }

    public DatagramPacket receiver (DatagramSocket ds, DatagramPacket dp) {
        String[] split = {};
        try {
            ds.receive(dp);
            split = decode(dp.getData());

            this.writer.println("rcv " + (System.currentTimeMillis()-this.startTime) + " " + split[0] + " " + split[1] + " " + split[2] + " " + split[3]);
            //System.out.println("success receiving");

            return dp;
        } catch (Exception e) {
            return dp;//receiver(ds, dp);
        }
    }

    public boolean pld() {
        System.out.println(new Random(this.seed).nextFloat());
        if (new Random(this.seed).nextFloat() > this.pdrop) {
            return true;
        } else {
            return false;
        }
    }
}