import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.Random;

/*
 * Server to process ping requests over UDP. 
 * The server sits in an infinite loop listening for incoming UDP packets. 
 * When a packet comes in, the server simply sends the encapsulated data back to the client.
 */

public class Receiver {

    private int sourcePort;
    private int destPort;
    private String file;
    private int count;

    public Receiver() {}

    public static void main(String[] args) throws Exception {

        Receiver r = new Receiver();

        ServerSocket s = new ServerSocket(Integer.parseInt(args[0]));
        r.sourcePort = Integer.parseInt(args[0]);
        DatagramSocket socket = new DatagramSocket(r.sourcePort);
        r.file = args[1];
        r.count = 0;
        //System.out.println(r.file);

        r.handshake(socket);
        r.transporter(socket);
        System.out.println(r.count);
    }

    public void handshake (DatagramSocket socket) {
        DatagramPacket packet = new DatagramPacket (new byte[50], 50);
        int seqNum = Math.abs(new Random(System.currentTimeMillis()).nextInt(9999-1)+1);

        try {
            packet = receiver(socket, packet);
            String[] synack = decode(packet.getData());
            if (synack[0].equals("S")) {
                this.destPort = packet.getPort();
                byte[] dataToSend = encode("SA", seqNum, Integer.parseInt(synack[1])+1, this.sourcePort, this.destPort, "");
                socket.connect(new InetSocketAddress(packet.getAddress().getHostAddress(), packet.getPort()));
                DatagramPacket reply = new DatagramPacket(dataToSend, dataToSend.length, packet.getAddress(), packet.getPort());
                sender(socket, reply);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("error with everything");
        }

        try {
            packet = receiver(socket, packet);
            String[] ack = decode(packet.getData());
        } catch (Exception e) {
            System.out.println("error2");
        }
      
    }

    public void transporter(DatagramSocket ds) {
        byte[] chunk;
        String[] received;
        PrintWriter writer = null;
        String packetType;

        try {
            writer = new PrintWriter(this.file, "UTF-8");
        } catch (Exception e) {}

        int seqNum;
        int ackNum;
        DatagramPacket dp1 = new DatagramPacket(new byte[1000], 1000);
        DatagramPacket dp2;

        try {
            while (true) {
                dp1 = receiver(ds, dp1);
                received = decode(dp1.getData());
                if (received[0].equals("F")) {
                    //  System.out.println(received[0]);
                    writer.close();
                    teardown(ds, dp1);
                    return;
                }
                //os.write(received[4].getBytes());
                String[] split = received[6].split("\0");
                writer.print(split[0]);
                //System.out.print(received[4]);
                chunk = encode("A", Integer.parseInt(received[3]), Integer.parseInt(received[1])+Integer.parseInt(received[2]), this.sourcePort, this.destPort, "");
                //System.out.println(Arrays.toString(decode(chunk)));
                dp2 = new DatagramPacket(chunk, chunk.length, dp1.getAddress(), dp1.getPort());
                sender(ds, dp2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void teardown(DatagramSocket ds, DatagramPacket dp) {
        DatagramPacket packet = new DatagramPacket (new byte[50], 50);

        try {
            String[] synack = decode(dp.getData());
            byte[] dataToSend1 = encode("A", Integer.parseInt(synack[3]), Integer.parseInt(synack[1])+1, this.sourcePort, this.destPort, "");
            DatagramPacket reply1 = new DatagramPacket(dataToSend1, dataToSend1.length, dp.getAddress(), dp.getPort());
            sender(ds, reply1);
            byte[] dataToSend2 = encode("F", Integer.parseInt(synack[3]), Integer.parseInt(synack[1])+1, this.sourcePort, this.destPort, "");
            DatagramPacket reply2 = new DatagramPacket(dataToSend2, dataToSend2.length, dp.getAddress(), dp.getPort());
            sender(ds, reply2);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error with everything");
        }

        try {
            packet = receiver(ds, packet);
            String[] ack = decode(packet.getData());
            if (ack[0].equals("A")) ds.close();
        } catch (Exception e) {
            System.out.println("error2");
        }
    }

    public void sender (DatagramSocket s, DatagramPacket p) {
        try {
            s.send(p);
            //System.out.println("success sending");
            return;
        } catch (Exception e) {
            System.out.println("error sending function");
            sender(s, p);
            return;
        }
    }

    public DatagramPacket receiver (DatagramSocket ds, DatagramPacket dp) {
        try {
            ds.receive(dp);
            String[] split = decode(dp.getData());
            System.out.println(split[1]);
            this.count++;
            return dp;
        } catch (Exception e) {
            return receiver(ds, dp);
        }
    }

    public byte[] encode(String type, int seqNum, int ackNum, int sourcePort, int destPort, String data) {
        //System.out.println(new String(type + " " + Integer.toString(seqNum) + " " + Integer.toString(data.length()) + " " + Integer.toString(ackNum)).getBytes());
        //System.out.println(new String(type + " " + Integer.toString(seqNum) + " " + Integer.toString(data.length()) + " " + Integer.toString(ackNum) + " " + Integer.toString(sourcePort) + " " + Integer.toString(destPort)).getBytes().length);
        return new String(type + " " + Integer.toString(seqNum) + " " + Integer.toString(data.length()) + " " + Integer.toString(ackNum) + " " + Integer.toString(sourcePort) + " " + Integer.toString(destPort) + " " + data).getBytes();
    }

    public String[] decode(byte[] packet) {
        return new String(packet).split(" ", 7);
    }
}